from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.decorators.cache import cache_page

from common.views import GlobalSeacrh

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/v1/", include("service.urls")),
    path("api/v1/", include("accounts.urls")),
    path("search/", cache_page(15)(GlobalSeacrh.as_view())),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += [path("__debug__/", include(debug_toolbar.urls))]
