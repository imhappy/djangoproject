from django.contrib import admin
from service import models
from django.db.models import QuerySet
from django.shortcuts import render
import json
from django.core.serializers.json import DjangoJSONEncoder
from service.services import BookService

"""Registration of all Models into admin panel"""


@admin.register(models.Writer)
class WriterAdmin(admin.ModelAdmin):
    list_display = ["id", "full_name"]
    readonly_fields = ["id", "created_at"]
    fields = ["full_name"]


@admin.register(models.Genre)
class GenreAdmin(admin.ModelAdmin):
    list_display = ["id", "name"]
    readonly_fields = ["id", "created_at"]
    fields = ["name"]


@admin.register(models.Review)
class ReviewAdmin(admin.ModelAdmin):
    list_display = ["id", "text", "rate"]
    readonly_fields = ["id", "created_at"]
    fields = ["text", "rating", "book"]

    raw_id_fields = ["book"]

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.select_related("book")
        return queryset


@admin.register(models.Book)
class BookAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "image",
        "title",
        "description",
        "price",
        "promotion",
        "writer",
        "genre",
        "published",
        "owner",
    ]
    readonly_fields = ["id", "created_at"]
    fields = [
        "image",
        "title",
        "description",
        "price",
        "promotion",
        "genre",
        "writer",
        "published",
        "owner",
    ]

    raw_id_fields = ["writer", "genre", "owner"]

    """Statistic"""

    def statistic(self, request):
        labels = BookService.get_labels()
        data = BookService.get_data()
        context = {
            "labels": json.dumps(labels, cls=DjangoJSONEncoder),
            "data": json.dumps(data, cls=DjangoJSONEncoder),
        }
        return render(request, "admin/review_line_chart.html", context=context)

    def get_urls(self):
        urls = super().get_urls()
        from django.urls import path

        custom_urls = [path("statistics/", self.statistic)]
        return custom_urls + urls

    """Getting queryset for ForeingKey relations"""

    def get_queryset(self, request) -> QuerySet[models.Book]:
        queryset = super().get_queryset(request)
        queryset = queryset.select_related("writer", "genre", "owner")
        return queryset
