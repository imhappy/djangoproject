from django.urls import path
from accounts import views

urlpatterns = [
    path("set/seller/", views.SellerSetAPIView.as_view(), name="seller-setting"),
    path(
        "register/buyer/", views.RegistrationBuyerAPI.as_view(), name="register-buyer"
    ),
    path(
        "register/admin/", views.RegistrationAdminAPI.as_view(), name="register-admin"
    ),
    path("register/confirm/", views.ConfirmAPIView.as_view(), name="confirm"),
    path(f"login/google/", views.GoogleSocialAuthView.as_view()),
    path("login/", views.LoginAPI.as_view()),
]
