from accounts.models import User
from rest_framework_simplejwt.tokens import RefreshToken
from google.auth.transport import requests
from google.oauth2 import id_token

"""Google Auth Service"""


class GoogleService:
    @staticmethod
    def validate(auth_token):
        try:
            idinfo = id_token.verify_oauth2_token(auth_token, requests.Request())

            if "accounts.google.com" in idinfo["iss"]:
                return idinfo

        except:
            return "The token is either invalid or has expired"


"""User Service"""


class UserService:
    __user_model = User

    @classmethod
    def get_emails(cls, **kwargs):
        return (
            cls.__user_model.objects.filter(**kwargs)
            .order_by("-created_at")
            .only("email")
        )

    @classmethod
    def get_list(cls, **kwargs):
        return (
            cls.__user_model.objects.filter(**kwargs)
            .order_by("-created_at")
            .only(
                "id",
                "email",
                "username",
                "role",
                "is_active",
                "is_verified",
                "is_staff",
                "auth_provider",
            )
        )

    @staticmethod
    def tokens(user):
        refresh = RefreshToken.for_user(user)
        return {"refresh": str(refresh), "access": str(refresh.access_token)}
