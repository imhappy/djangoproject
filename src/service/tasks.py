from celery import shared_task
import time


@shared_task
def sending_mails():
    from service.utils import send_promoted_books_via_gmail

    time.sleep(15)
    send_promoted_books_via_gmail()
