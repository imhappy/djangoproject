from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter
from rest_framework import generics, response, status
from rest_framework.pagination import PageNumberPagination
from service import filters
from service import services, serializers
from service import permissions

"""Views For Books"""


class BookAPIView(generics.ListCreateAPIView):
    serializer_class = serializers.BookListSerializer
    pagination_class = PageNumberPagination
    queryset = services.BookService.get_list(is_deleted=False)
    permission_classes = [permissions.IsSellerAndAdminOrReadOnly]

    # filters for books
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ("genre_id", "writer_id")
    search_fields = ["title", "published"]
    filter_class = filters.BookFilter

    def get(self, request, *args, **kwargs) -> response.Response:
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return response.Response(serializer.data)

    def post(self, request, *args, **kwargs) -> response.Response:
        serializer = serializers.BookCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = services.BookService.create_book(
            image=serializer.validated_data.get("image"),
            title=serializer.validated_data.get("title"),
            description=serializer.validated_data.get("description"),
            price=serializer.validated_data.get("price"),
            promotion=serializer.validated_data.get("promotion"),
            genre=serializer.validated_data.get("genre"),
            writer=serializer.validated_data.get("writer"),
            published=serializer.validated_data.get("published"),
        )
        return response.Response(
            data=serializers.BookCreateSerializer(data).data,
            status=status.HTTP_201_CREATED,
        )


class BookDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = services.BookService.get_list(is_deleted=False)
    serializer_class = serializers.BookDetailSerializer
    lookup_field = "id"
    permission_classes = [permissions.IsOwnerOrReadOnly]

    def get(self, request, *args, **kwargs) -> response.Response:
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return response.Response(data=serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs) -> response.Response:
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return response.Response(data=serializers.BookDetailSerializer(instance).data)


"""VIEW for promotions"""


class PromotionAPIView(generics.ListAPIView):
    serializer_class = serializers.BookListSerializer
    pagination_class = PageNumberPagination
    queryset = services.BookService.get_list(is_deleted=False, promotion=True)

    def get(self, request, *args, **kwargs) -> response.Response:
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return response.Response(serializer.data)


"""View for Genre ```CRUD```"""


class GenreAPIView(generics.ListCreateAPIView):
    serializer_class = serializers.GenreSerializer
    pagination_class = PageNumberPagination
    queryset = services.GenreService.get_list(is_deleted=False)
    filter_backends = [SearchFilter]
    search_fields = ["name"]

    def get(self, request, *args, **kwargs) -> response.Response:
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return response.Response(serializer.data)

    def post(self, request, *args, **kwargs) -> response.Response:
        serializer = serializers.GenreSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = services.GenreService.create_genre(
            name=serializer.validated_data["name"]
        )
        return response.Response(
            data=serializers.GenreSerializer(data).data,
            status=status.HTTP_201_CREATED,
        )


class GenreDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = services.GenreService.get_list(is_deleted=False)
    pagination_class = PageNumberPagination
    serializer_class = serializers.GenreSerializer
    lookup_field = "id"

    def get(self, request, *args, **kwargs) -> response.Response:
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return response.Response(data=serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs) -> response.Response:
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return response.Response(data=serializers.GenreSerializer(instance).data)


"""View for Writers ```CRUD```"""


class WriterAPIView(generics.ListCreateAPIView):
    queryset = services.WriterService.get_list(is_deleted=False)
    pagination_class = PageNumberPagination
    serializer_class = serializers.WriterSerializer
    filter_backends = [SearchFilter]
    search_fields = ["full_name"]

    def get(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return response.Response(serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = serializers.WriterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = services.WriterService.create_writer(
            full_name=serializer.validated_data["full_name"]
        )
        return response.Response(
            data=serializers.WriterSerializer(data).data,
            status=status.HTTP_201_CREATED,
        )


class WriterDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = services.WriterService.get_list(is_deleted=False)
    pagination_class = PageNumberPagination
    serializer_class = serializers.WriterSerializer
    lookup_field = "id"

    def get(self, request, *args, **kwargs) -> response.Response:
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return response.Response(data=serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs) -> response.Response:
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return response.Response(data=serializers.WriterSerializer(instance).data)
