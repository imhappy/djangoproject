import csv
from django.core.management.base import BaseCommand

from accounts.models import User
from service.models import Book, Genre, Writer
import re
from random import randint


class Command(BaseCommand):
    help = "Unload books data from CSV file"

    def add_arguments(self, parser):
        parser.add_argument("csv_file", help="Path to the CSV file")

    def handle(self, *args, **options):
        csv_file = options["csv_file"]
        with open(csv_file, "r") as file:
            reader = csv.reader(file)
            next(reader)
            for row in reader:
                title = row[2]
                writer_name = row[4].split("; ")[0]
                genre_name = row[5].split(", ")[0]
                description = row[7]
                published = row[8] if not "" else 1000
                price = randint(200, 1000)

                genres = Genre.objects.filter(name=genre_name)
                if not genres.exists():
                    Genre.objects.create(name=genre_name)

                writers = Writer.objects.filter(full_name=writer_name)
                if not writers.exists():
                    Writer.objects.create(full_name=writer_name)

                # books = Book.objects.filter(title=title)
                # if books.exists():
                #     self.stdout.write(
                #         self.style.WARNING(
                #             f"Duplicate books with title = {title} found"
                #         )
                #     )
                #     self.stdout.write(self.style.ERROR("Data uploading is stopped !!"))
                #     break

                writer = Writer.objects.get(full_name=writer_name)
                genre = Genre.objects.get(name=genre_name)
                user = User.objects.get(username="admin")

                book = Book.objects.create(
                    title=title,
                    writer=writer,
                    genre=genre,
                    promotion=False,
                    description=description,
                    price=price,
                    published=published,
                    owner=user,
                )
                self.stdout.write(self.style.SUCCESS(f'Book "{book}" created.'))
        self.stdout.write(self.style.SUCCESS("Data unloading completed."))
