from django.db.models import Avg
from service import models
from django.db.models import QuerySet


class BookService:
    __book_model = models.Book
    __genre_model = models.Genre
    __writer_model = models.Writer

    @classmethod
    def get_labels(cls):
        books = cls.__book_model.objects.order_by("-created_at")[:10]
        return [book.title for book in books]

    @classmethod
    def get_data(cls):
        books = cls.__book_model.objects.order_by("-created_at")[:10]
        return [
            book.books_reviews.aggregate(avg_rating=Avg("rate")).get("avg_rating")
            for book in books
        ]

    @classmethod
    def get_list(cls, **kwargs) -> QuerySet[models.Book]:
        return (
            cls.__book_model.objects.filter(**kwargs)
            .order_by("-published")
            .only(
                "id",
                "title",
                "description",
                "promotion",
                "price",
                "genre",
                "writer",
                "published",
            )
            .select_related("writer", "genre")
        )

    @classmethod
    def create_book(
        cls,
        image,
        title: str,
        description: str,
        promotion: bool,
        price: float,
        genre: str,
        writer: str,
        published: int,
    ):
        return cls.__book_model.objects.create(
            image=image,
            title=title,
            description=description,
            promotion=promotion,
            price=price,
            genre_id=genre,
            writer_id=writer,
            published=published,
        )


class GenreService:
    __genre_model = models.Genre

    @classmethod
    def get_list(cls, **kwargs) -> QuerySet[models.Genre]:
        return (
            cls.__genre_model.objects.filter(**kwargs)
            .order_by("-created_at")
            .only("id", "name")
        )

    @classmethod
    def create_genre(cls, name):
        return cls.__genre_model.objects.create(name=name)


class WriterService:
    __writer_model = models.Writer

    @classmethod
    def get_list(cls, **kwargs):
        return (
            cls.__writer_model.objects.filter(**kwargs)
            .order_by("-created_at")
            .only("id", "full_name")
        )

    @classmethod
    def create_writer(cls, full_name):
        return cls.__writer_model.objects.create(full_name=full_name)
