from django.urls import path
from service import views
from django.views.decorators.cache import cache_page

urlpatterns = [
    # path for books
    path("books/", cache_page(15)(views.BookAPIView.as_view())),
    path("books/<uuid:id>/", cache_page(15)(views.BookDetailAPIView.as_view())),
    path("books/promotion/", cache_page(15)(views.PromotionAPIView.as_view())),
    # path for genres
    path("genres/", cache_page(15)(views.GenreAPIView.as_view())),
    path("genres/<uuid:id>/", cache_page(15)(views.GenreDetailAPIView.as_view())),
    # path for writers
    path("writers/", cache_page(15)(views.WriterAPIView.as_view())),
    path("writers/<uuid:id>/", cache_page(15)(views.WriterAPIView.as_view())),
]
