from service.models import Book, Genre, Writer
from accounts.models import User
from service.serializers import BookListSerializer, WriterSerializer, GenreSerializer
from accounts.serializers import LoginSerializer
from django.db.models import Q


class GlobalSearchMixin:
    def global_querylist(self, qeurylist, search):
        qeurylist = (
            {
                "queryset": Book.objects.filter(
                    Q(title__icontains=search) | Q(price__icontains=search)
                ),
                "serializer_class": BookListSerializer,
            },
            {
                "queryset": User.objects.filter(Q(username__icontains=search)),
                "serializer_class": LoginSerializer,
            },
            {
                "queryset": Genre.objects.filter(Q(name__icontains=search)),
                "serializer_class": GenreSerializer,
            },
            {
                "queryset": Writer.objects.filter(Q(full_name__icontains=search)),
                "serializer_class": WriterSerializer,
            },
        )

        return qeurylist
