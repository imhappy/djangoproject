from django_filters import rest_framework as filters
from service.models import Book


class BookFilter(filters.FilterSet):
    price = filters.RangeFilter()
    min_price = filters.NumberFilter(name="price", lookup_type="gte")
    max_price = filters.NumberFilter(name="price", lookup_type="lte")

    class Meta:
        model = Book
        fields = ["price", "min_price", "max_price"]
