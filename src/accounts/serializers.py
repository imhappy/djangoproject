from accounts.models import ConfirmCode, User
from rest_framework import serializers
from zxcvbn import zxcvbn
from rest_framework.exceptions import ValidationError
from rest_framework import serializers
from accounts.services import GoogleService
from accounts.utils import register_social_user

"""Google-AUTH serializer"""


class GoogleSocialAuthSerializer(serializers.Serializer):
    auth_token = serializers.CharField()

    def validate_auth_token(self, auth_token):
        user_data = GoogleService.validate(auth_token)
        print(user_data)
        try:
            user_data["sub"]
        except:
            raise serializers.ValidationError(
                "The token is invalid or expired. Please login again."
            )

        user_id = user_data["sub"]
        email = user_data["email"]
        name = user_data["name"]
        provider = "google"

        return register_social_user(
            provider=provider, user_id=user_id, email=email, name=name
        )


"""Register Serializer"""


class RegisterSerializer(serializers.Serializer):
    username = serializers.CharField()
    email = serializers.EmailField()
    password = serializers.CharField(max_length=16)

    def validate_email(self, email):
        try:
            User.objects.get(email=email)
        except User.DoesNotExist:
            return email
        raise ValidationError("User already exists!")

    def validate_password(self, password):
        result = zxcvbn(password)["score"]
        if result < 3:
            raise ValidationError("Use hints for making password!")
        return password

    def validate_username(self, username):
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise ValidationError("User already exists!")


"""Confirm Serializer"""


class ConfirmCodeSerializer(serializers.Serializer):
    user_id = serializers.IntegerField(required=False)
    code = serializers.CharField(min_length=6, max_length=6)

    def validate_user_id(self, user_id):
        try:
            ConfirmCode.objects.get(id=user_id)
        except ConfirmCode.DoesNotExist:
            raise ValidationError(f"User with id ({user_id}) not found")
        return user_id


"""Login serialzer"""


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()
