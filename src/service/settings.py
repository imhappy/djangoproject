from datetime import date

"""Validator for published field"""
TODAY_YEAR = date.today().year


"""Rate field choices"""
RATING = (
    (1, "worst"),
    (2, "bad"),
    (3, "not bad"),
    (4, "cool"),
    (5, "wonderful"),
)
