from django.contrib.auth import authenticate
from rest_framework import generics

from accounts.models import ConfirmCode, User, SellerCode
from accounts.serializers import (
    RegisterSerializer,
    LoginSerializer,
    ConfirmCodeSerializer,
)
from accounts.services import UserService
from accounts.settings import activate_code
from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from accounts.serializers import GoogleSocialAuthSerializer


"""Seller Setting"""


class SellerSetAPIView(generics.ListCreateAPIView):
    queryset = UserService.get_list(is_deleted=False)
    serializer_class = ConfirmCodeSerializer

    def get(self, request, *args, **kwargs):
        if not request.user.is_anonymous:
            if request.user.role not in (1, 3):
                code = SellerCode.objects.create(
                    user_id=request.user.id, code=activate_code
                )
                return Response(
                    status=status.HTTP_200_OK,
                    data={"user_id": code.user_id, "code": code.code},
                )
            else:
                return Response(data={"message": "something went wrong"})
        else:
            return Response(data={"message": "Please authorize!"})

    def post(self, request, *args, **kwargs):
        serializer = ConfirmCodeSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if SellerCode.objects.filter(code=request.data["code"]):
            User.objects.update(role=2)
            return Response(
                status=status.HTTP_202_ACCEPTED,
                data={"success": "confirmed", "role": request.user.role},
            )

        return Response(
            status=status.HTTP_406_NOT_ACCEPTABLE,
            data={"error": "something went wrong"},
        )


"""ADMIN REGISTRATION"""


class RegistrationAdminAPI(generics.CreateAPIView):
    serializer_class = RegisterSerializer
    queryset = UserService.get_list(is_deleted=False)

    def post(self, request, *args, **kwargs):
        serializer = RegisterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        username = serializer.validated_data.get("username")
        password = serializer.validated_data.get("password")
        email = serializer.validated_data.get("email")
        user = User.objects.create_superuser(
            username=username,
            password=password,
            email=email,
        )
        return Response(
            status=status.HTTP_201_CREATED, data={"user_id": user.id, "role": user.role}
        )


"""BUYER REGISTRATION"""


class RegistrationBuyerAPI(generics.CreateAPIView):
    queryset = UserService.get_list(is_deleted=False)
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        serializer = RegisterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        username = serializer.validated_data.get("username")
        password = serializer.validated_data.get("password")
        email = serializer.validated_data.get("email")
        user = User.objects.create_user(
            username=username, password=password, role=3, email=email
        )
        code = ConfirmCode.objects.create(user_id=user.id, code=activate_code)
        return Response(
            status=status.HTTP_201_CREATED,
            data={"user_id": user.id, "role": user.role, "code": code.code},
        )


"""Sending Confirmation Code"""


class ConfirmAPIView(generics.CreateAPIView):
    queryset = UserService.get_list(is_deleted=False)
    serializer_class = ConfirmCodeSerializer

    def post(self, request, *args, **kwargs):
        serializer = ConfirmCodeSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        if ConfirmCode.objects.filter(code=request.data["code"]):
            User.objects.update(is_verified=True)
            return Response(
                status=status.HTTP_202_ACCEPTED, data={"success": "confirmed"}
            )

        return Response(
            status=status.HTTP_406_NOT_ACCEPTABLE,
            data={"error": "something went wrong"},
        )


"""Login to get JWT-tokens"""


class LoginAPI(generics.CreateAPIView):
    serializer_class = LoginSerializer
    queryset = UserService.get_list(is_deleted=False)

    def post(self, request, *args, **kwargs):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        email = serializer.validated_data["email"]
        password = serializer.validated_data["password"]

        user = authenticate(email=email, password=password)
        if user:
            return Response(data=UserService.tokens(user=user))

        return Response(
            status=status.HTTP_401_UNAUTHORIZED,
            data={"error": "Email or password wrong!"},
        )


"""Google login-reg-auth"""


class GoogleSocialAuthView(GenericAPIView):
    serializer_class = GoogleSocialAuthSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = (serializer.validated_data)["auth_token"]
        return Response(data, status=status.HTTP_200_OK)
