from django.conf import settings
from django.core.mail import send_mail
from accounts.services import UserService
from service.services import BookService


def send_promoted_books_via_gmail():
    subject = "Promoted Books!!! BIG SALE"
    queryset = BookService.get_list(is_deleted=False, promotion=True).order_by(
        "-updated_at"
    )[:6]
    books = [book.title for book in queryset]
    message = f"{books} These books were promoted"
    from_email = settings.EMAIL_HOST
    emails = UserService.get_emails(role=3)
    return send_mail(
        subject=subject,
        message=message,
        from_email=from_email,  # Отправитель
        recipient_list=emails,
        fail_silently=False,  # Опциональный параметр для обработки ошибок
    )
