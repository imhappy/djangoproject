ROLE_CHOICES = ((1, "admin"), (2, "seller"), (3, "buyer"))

AUTH_PROVIDERS = {
    "email": "email",
    "google": "google",
}

from random import choices

activate_code = "".join(choices("0123456789", k=6))
